#!/bin/sh
# killme.sh
# deletes a user's .ssh folder signifying they have been eliminated
# run by the user's account, hopefully without their control
# This should only be run by users who have been eliminated, obviously

SLBRUSER=$(whoami)
test -z "$SLBRUSER" && echo "no user provided" && exit
if ! (grep -qE "^$SLBRUSER$" /home/slbr/users.txt)
then
	echo "user $SLBRUSER not found in users.txt, will not delete"
	exit
fi

echo "removing /home/slbr/users/$SLBRUSER"
rm -rf "/home/slbr/users/$SLBRUSER/.ssh"
lxc delete -f "$SLBRUSER"
