#!/bin/sh
# deathwatch.sh
# efficiently checks each user directory to see if the .ssh folder has been deleted
# deletes the user if it has

inotifywait -mre delete users | while read -r dir
do
	user="$(echo "$dir" | cut -f 2 -d '/')"
	echo  "$dir" | grep -q 'ISDIR .ssh' \
		&& ./deluser.sh "$(echo "$dir" | cut -f 2 -d '/')" \
		&& ./sendmsg.sh "$user has been eliminated!"
done
