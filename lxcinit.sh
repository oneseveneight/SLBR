#!/bin/sh
# lxcinit.sh
# called from newuser.sh to set up an lxc container with the proper files

echo "	Launching container for user $(whoami)"
. /etc/profile.d/apps-bin-path.sh \
	&& lxc launch ubuntu: "$(whoami)"
lxc file push /home/slbr/tmuxinit.sh "$(whoami)"/usr/bin/tmuxinit.sh
lxc file push /home/slbr/suicidebash.bashrc "$(whoami)"/etc/bash.bashrc
