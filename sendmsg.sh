#!/bin/sh
# sendmsg.sh
# sends a message to all conatiners

ADDRESSES=$(lxc list | cut -f 4 -d '|' \
	| grep -Eo "[0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*")
for ADDRESS in $ADDRESSES
do
	echo "$1" | nc -q2 "$ADDRESS" 9090
done
