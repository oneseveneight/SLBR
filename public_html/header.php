<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="tildeverse suicide linux battle royale">
    <meta name="author" content="Ben Harris">
    <title><?=$title ?? "slbr.tildeverse.org"?></title>

    <link rel="stylesheet" href="https://tilde.team/css/hacker.css">
    <link rel="stylesheet" href="https://tilde.team/css/fork-awesome.css">

    <?=$additional_head ?? ""?>
    <?php unset($title); unset($additional_head); ?>
</head>

<body style="padding-top: 70px;">
    <div class="container">
