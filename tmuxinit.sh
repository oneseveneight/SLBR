#!/bin/sh
# tmuxinit.sh
# sets up a tmux session within the user's container

tmux new-session -d -n test
tmux select-window -t 0:0    
tmux send -t 0:0 'nc -lkp 9090' ENTER
tmux split-window -h bash
tmux select-layout    
tmux resize-pane -t 1 -L 60
tmux a
