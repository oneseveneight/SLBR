#!/bin/sh
# login.sh
# the script that runs when a player logs into their account
# will put the player in their container and setup a tmux session for them
# will also run killme.sh if called from a container
# a bit hacky, perhaps we should find a better method?

. /etc/profile.d/apps-bin-path.sh
SLBRUSER="$(whoami)"
lxc list | grep -q "$(echo "$SSH_CLIENT" | cut -f 1 -d ' ')" && ~/killme.sh && exit 0
echo "The game has not started yet! Grab a cup of tea, or just wait here."
while test -z "$(cat /home/slbr/gamestatus)" || break
do
	printf "."
	sleep 1
done
echo "READY.......BEGIN!"
sleep 1
echo "Setting up your container..."
lxc exec "$SLBRUSER" ssh-keygen -- -t rsa -N '' -f /root/.ssh/id_rsa
lxc file pull "$SLBRUSER/root/.ssh/id_rsa.pub" "/home/slbr/users/$SLBRUSER/pubkey.temp"
cat "/home/slbr/users/$SLBRUSER/pubkey.temp" >> "/home/slbr/users/$SLBRUSER/.ssh/authorized_keys" && rm "/home/slbr/users/$SLBRUSER/pubkey.temp"
lxc exec "$SLBRUSER" /usr/bin/tmuxinit.sh
