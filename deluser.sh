#!/bin/sh
# deluser.sh
# deletes a user, their container, and their home direcory
# can be called manually or by a script

SLBRUSER=$1
test -z "$SLBRUSER" && echo "no user provided" && exit
if ! (grep -qE "^$SLBRUSER$" users.txt)
then
	echo "user $SLBRUSER not found in users.txt, will not delete"
	exit
fi

if ps -o stat= -p $PPID | grep -qv S+ #check if this is being called from deathwatch.sh
then
	echo "YOU ARE ABOUT TO PERMANENTLY DELETE USER $SLBRUSER AND ALL OF THEIR DATA. RE-ENTER THE USER'S NAME TO CONTINUE"
	read -r ENTEREDUSER
	[ "$ENTEREDUSER" != "$SLBRUSER" ] && echo "users did not match, exiting..." && exit
fi
lxc stop --force "$SLBRUSER" 
lxc delete --force "$SLBRUSER" 
sudo rm -rf "/home/slbr/users/$SLBRUSER" 
sudo userdel -f "$SLBRUSER" 
sed -i "s/^$SLBRUSER$//g" users.txt 
sed -i '/^$/d' users.txt
