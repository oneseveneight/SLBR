#!/bin/sh
# newuser.sh
# creates a new player

SLBRUSER=$1
PUBKEY=$2
test -z "$SLBRUSER" && echo "no user provided" && exit
test -z "$PUBKEY" && echo "no key provided" && exit
PUBKEY="$(cat "$PUBKEY")"

cut -d: -f1 /etc/passwd | grep -qx "$SLBRUSER" && echo "user already exists" && exit

echo "Creating homedir..."
mkdir "/home/slbr/users/$SLBRUSER"
mkdir "/home/slbr/users/$SLBRUSER/.ssh"
echo "Copying key..."
echo "$PUBKEY" >> "/home/slbr/users/$SLBRUSER/.ssh/authorized_keys"
echo "Creating container..."
echo "lxc exec $SLBRUSER bash" > "/home/slbr/users/$SLBRUSER/.profile"
chmod -w "/home/slbr/users/$SLBRUSER/.profile"
echo "Creating user..."
sudo useradd "$SLBRUSER" -d "/home/slbr/users/$SLBRUSER"
sudo usermod -a -G lxd "$SLBRUSER"
sudo cp ./killme.sh "/home/slbr/users/$SLBRUSER/killme.sh"
sudo chmod +x "/home/slbr/users/$SLBRUSER"
sudo chown -R "$SLBRUSER:$SLBRUSER" "/home/slbr/users/$SLBRUSER"
sudo chsh "$SLBRUSER" -s /home/slbr/login.sh
echo "Generating user keypair..."
sudo su "$SLBRUSER" -s /bin/sh -c "ssh-keygen -t rsa -N '' -f /home/slbr/users/$SLBRUSER/.ssh/id_rsa"
echo "Initializing container..."
sudo su "$SLBRUSER" -s /bin/sh -c "/home/slbr/lxcinit.sh"
echo "$SLBRUSER" >> users.txt
