# SUICIDE LINUX BATTLE ROYALE
coming soon! check #slbr on irc for updates!

typo? you're out.

when you play, you'll be dropped in a fresh vm. on the way to your goal, any mistyped command will delete your vm and you'll be eliminated.

contact ~ben or sose on tilde.chat (in the #slbr channel) to sign up.

## Prerequisites
- LXC
- LXD with an lxdbr0 device configured
- inotifywait
- BSD style netcat
- Tmux

## Setup
- Switch to the directory you would like to run SLBR in.
- `git clone https://tildegit.org/tildeverse/SLBR`
- `cd SLBR`
- `ln -s $PWD/* ..`
- `cd ..`

## Running the game
- Run `./deathwatch.sh` to start the death listener.
- Run `./newuser.sh [user] [ssh public key]` to create new users as you see fit.
- Run `./deluser.sh [user]` to delete a user. (this is done automatically when a user types a command incorrectly)
- Once all of your users are created and logged in, run `echo "1" > gamestatus` to start the game.

## TODO
- Install-time configuration (setting proper ips, hostname, etc.)
- Fix user tracking
=======
- A goal and a way to check it

This is a ~ work in progress ~
